package QaAutomation1;

import java.util.Scanner;

public class switchcase {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter a command");
		String text = input.nextLine();

		switch (text) {
		case "A":
			System.out.println("Appetizer");
			break;
		case "M":
			System.out.println("Main Course");
			break;
		case "S":
			System.out.println("Soup and Salad");
			break;
		case "D":
			System.out.println("Drinks");
			break;
		default:
			System.out.println("command not recognized");
		}
		
		input.close();
	}

}

package com.cerotid.walmart;

import java.util.List;

public class HelperClass {
	public static Cars getCarBasedOnCarType(String carType) throws Exception {
		Cars newCar = null;
		if (carType.equals("Camaro")) {
			newCar = new Camero();
		} else if (carType.equals("Challenger")) {
			newCar = new Challenger();
		} else if (carType.equals("Mustang")) {
			newCar = new Mustang();
		} else if (carType.equals("Coupes")) {
			newCar = new Coupe();
		} else if (carType.equals("Sedan")) {
			newCar = new Sedan();
		} else if (carType.equals("Hatchback")) {
			newCar = new Hatchback();
		} else if (carType.equals("Veloster")) {
			newCar = new Volester();
		} else if (carType.equals("Spark")) {
			newCar = new Spark();
		} else {
			throw new Exception("Car Type not available");
		}
		return newCar;
	}
	
	public static Cars getCarByVinNumber(String vin, List<Cars> cars) throws Exception {
		for (Cars c : cars) {
			if (c.getVin().equals(vin)) {
				return c;
			}
		}
		throw new Exception("Car doesn't exist");
	}
}




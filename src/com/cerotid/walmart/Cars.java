package com.cerotid.walmart;

public abstract class Cars {
	private Engine engine;
	private Tires tires;
	private int doors;
	private int maxSpeed;
	private boolean hasAndroidauto;
	private boolean hasCarPlay;
	private String Vin;
	private String LastoilChangeDate;
	private String LastTireChange;
	private String colors;

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Tires getTires() {
		return tires;
	}

	public void setTires(Tires tires) {
		this.tires = tires;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public boolean isHasAndroidauto() {
		return hasAndroidauto;
	}

	public void setHasAndroidauto(boolean hasAndroidauto) {
		this.hasAndroidauto = hasAndroidauto;
	}

	public boolean isHasCarPlay() {
		return hasCarPlay;
	}

	public void setHasCarPlay(boolean hasCarPlay) {
		this.hasCarPlay = hasCarPlay;
	}

	public String getVin() {
		return Vin;
	}

	public void setVin(String vin) {
		Vin = vin;
	}

	public String getLastoilChangeDate() {
		return LastoilChangeDate;
	}

	public void setLastoilChangeDate(String lastoilChangeDate) {
		LastoilChangeDate = lastoilChangeDate;
	}

	public String getColors() {
		return colors;
	}

	public void setColors(String colors) {
		this.colors = colors;
	}

	public String getLastTireChange() {
		return LastTireChange;
	}

	public void setLastTireChange(String lastTireChange) {
		LastTireChange = lastTireChange;
		
	}

}

package com.cerotid.walmart;


public class WalmartRevenue {
	private double revenue;

	public double getRevenue() {
		return revenue;
	}

	public void oilChange(Cars car) {
		if (car instanceof Coupe) {
			revenue += 49.99;

		} else if (car instanceof Sedan) {
			revenue += 39.99;

		} else if (car instanceof Hatchback) {
			revenue += 29.99;

		}
	}

}

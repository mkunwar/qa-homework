package com.cerotid.walmart;

import java.util.ArrayList;
import java.util.List;

public class Walmart {
	public static void main(String[] args) {
		List<Cars> cars = new ArrayList<Cars>();

		for (int i = 0; i < 20; i++) {
			if (i < 10) {
				cars.add(new Sedan());
			} else if (i < 12) {
				cars.add(new Spark());
			} else if (i < 16) {
				cars.add(new Volester());
			} else if (i < 18) {
				cars.add(new Mustang());
			} else if (i < 19) {
				cars.add(new Camero());
			} else {
				cars.add(new Challenger());
			}
		}
		WalmartRevenue revenue = new WalmartRevenue();
		for (Cars car : cars) {
			revenue.oilChange(car);

		}

		System.out.println(revenue.getRevenue());
	}

}
